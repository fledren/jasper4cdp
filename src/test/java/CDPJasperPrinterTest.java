import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CDPJasperPrinterTest {

    CDPJasperPrinter printer = new CDPJasperPrinter();

    @Test
    public void connectionIsUp() throws Exception {
        Assertions.assertNotNull(this.printer.getConnection(), "Connection is down");
    }

    @Test
    public void leRapportSImprime() throws Exception {
        this.printer.print(this.printer.getConnection(), "utilisateurs");
    }

}
