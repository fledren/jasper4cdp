import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import oracle.jdbc.driver.OracleDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CDPJasperPrinter {

    Connection getConnection() throws SQLException {
        DriverManager.registerDriver(new OracleDriver());
        return DriverManager.getConnection(Constants.DRIVER_DATABASE_DB,
                                           Constants.LOGIN_DATABASE_DB,
                                           Constants.PASSWORD_DATABASE_DB);
    }

    void print(Connection connection, String report) throws Exception {
        JasperPrint jasperPrint = JasperFillManager.fillReport(
                                                new FileInputStream(new File(Constants.PATH_FOR_JASPER_REPORTS, report + ".jasper")),
                                                null,
                                                connection);
        FileOutputStream outputStream = new FileOutputStream(new File("C:/temp/report.pdf"));
        outputStream.write(JasperExportManager.exportReportToPdf(jasperPrint));

    }

}
